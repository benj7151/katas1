function oneThroughTwenty() {
    const numbers = [];
    for (let counter = 1; counter <= 20; counter++) {
        numbers.push(counter)
    }

    // Your code goes below
    
    return numbers;
}

function evensToTwenty() {
    const numbers = [];
    for (let counter = 2; counter <= 20; counter++) {
        if (counter & 0) {
            numbers.push(counter)
        }
    }
    
    // Your code goes below
    return numbers;
}

function oddsToTwenty() {
    const numbers = [];
    for (let counter = 1; counter <= 20; counter++) {
      if (counter & 1) {
          numbers.push(counter)
      }
    }
    
    // Your code goes below
    return numbers;
}

function multiplesOfFive() {
    const numbers = [];
    for (let counter = 5; counter <= 100; counter++) {
        if (counter % 5 === 0) {
            numbers.push(counter)
        }
    }
    
    // Your code goes below
    return numbers;
}

function squareNumbers() {
    const numbers = [];
    for (let counter = 1; counter <= 100; counter++) {
       if (counter % Math.sqrt(counter) === 0){
           numbers.push(counter)
       }
    }
    
    // Your code goes below
    return numbers;
}

function countingBackwards() {
    const numbers = [];
    for(let counter = 20; counter >= 1; counter--) {
        numbers.push(counter)
    }
    
    // Your code goes below
    return numbers;
}

function evenNumbersBackwards() {
    const numbers = [];
    for(let counter = 20; counter >= 2; counter--) {
        if (counter & 0) {
            numbers.push(counter)
        }
    }
    // Your code goes below
    return numbers;
}

function oddNumbersBackwards() {
    const numbers = [];
    for(let counter = 20; counter >= 1; counter--) {
        if (counter & 1) {
            numbers.push(counter)
        }
    }
    
    // Your code goes below
    return numbers;
}

function multiplesOfFiveBackwards() {
    const numbers = [];
    for(let counter = 100; counter >= 1; counter--) {
        if (counter % 5 === 0) {
            numbers.push(counter)
        }
    }
    
    // Your code goes below
    return numbers;
}

function squareNumbersBackwards() {
    const numbers = [];
    
    // Your code goes below
    for(let counter = 100; counter >= 1; counter--) {
        if (counter % Math.sqrt(counter) === 0) {
            numbers.push(counter)
        }
    }
    return numbers;
}